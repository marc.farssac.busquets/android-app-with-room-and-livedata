CHANGELOG
=========

Release v0.1 (Sunday 05.04.2020)
================================
`[ac-01]` Added Room Database and Data Access Object
`[ac-02]` Added a MVVM to the App
`[ac-03]` Branch merging ac-01 and ac-2
`[ac-04]` Added Activity to let user add new words to the Database

Master
======
05.04.2020 Initial branch

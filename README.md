# Android Room Architecture Components project with GitFlow

This is a simple App that illustrates the process of creation of a simple App accessing a local DB using the Room Architecture Component with LiveData. It updated the UI using the Architecture Component ViewModel.

## Branching model

This project follows the git flow branching model. Branches are created for each of the features to be implemented.

### Branches list

- ac-01-room : Adds Room and DAO functionality
- ac-02-mvvm : Adds the UI and a RecyclerView to the MainActivity
- ac-03-merge-branch-01-and-02 : The Word class was developed in branch 01 but also needed in branch-02. To avoid latter merge issues, a new branch merging the previous has been created.

## About

Project crafted by Marc Farssac

## Acknowledgments

Thanks to Stackoverflow, GitFlow and the Google Android Developers documentation